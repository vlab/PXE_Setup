#!/bin/bash
source $(dirname $0)/config.source
echo "Restarting nfs server"
sudo systemctl restart nfs-server

echo "Checking for running tftpd"
TFTPD_PID=$(pidof in.tftpd)
if [ ! -z "$TFTPD_PID" ]; then
	echo "Killing previous TFTP deamon"
	sudo killall in.tftpd
fi

echo "Configuring DNSMASQ"
echo "interface=$ETH_IFACE \
bind-interfaces \
dhcp-range=$ETH_RANGE,proxy \
pxe-service=0,$PXE_NAME" > dnsmasq.conf
echo "interface=$ETH_IFACE
bind-interfaces
dhcp-range=$ETH_RANGE,proxy
pxe-service=0,$PXE_NAME" > dnsmasq.conf

echo "Starting new TFTP daemon"
sudo /usr/sbin/in.tftpd --listen --user $USER --address :69 --ipv4 --secure --create $TFTPBOOT_DIR

echo "Checking for running dnsmasq"
DNSMASQ_PID=$(pidof dnsmasq)
if [ ! -z "$DNSMASQ_PID" ]; then
        echo "Killing previous DNSMASQ deamon"
#        sudo killall dnsmasq
fi
echo "Starting dnsmasq"
sudo dnsmasq -C dnsmasq.conf
