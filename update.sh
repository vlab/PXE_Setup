#!/bin/bash
source $(dirname $0)/config.source
if [ ! $# -eq 3 ]; then
	echo "Usage: $0 <Yocto output dir> <target> <Machine type>"
	exit 1
fi
META_OUTPUT=$1
TARGET=$2
MACHINE_TYPE=$3
BUILD_DIR="$META_OUTPUT/build/tmp/deploy/images/$MACHINE_TYPE"

if [ ! -d "$BUILD_DIR" ]; then
  echo "$BUILD_DIR does not exist. Maybe you specified the wrong Yocto directory."
  exit 1
fi

ORG_DIR=$PWD
SCRIPT_DIR=$(dirname $0)
TMP_DIR=$(mktemp -d)
BACKUP_DIR=$NFS_DIR/$NFS_SUB_NAME_$(date +%d_%m_%Y-%H-%M-%S).back

mkdir -p $TMP_DIR/rootfs

echo "Extracting Rootfs"
tar -xf $BUILD_DIR/$TARGET-$MACHINE_TYPE.rootfs.tar.bz2 -C $TMP_DIR/rootfs

echo "Copying boot files"
tar -xf $BUILD_DIR/$TARGET-$MACHINE_TYPE.bootfs.tar -C $TMP_DIR/rootfs/boot/
echo "enable_uart=1
dtoverlay=vc4-kms-v3d" > $TMP_DIR/rootfs/boot/config.txt
echo "console=serial0,115200 console=tty root=/dev/nfs nfsroot=$NFS_HOST:$NFS_DIR/$NFS_SUB_NAME,vers=3 rw ip=dhcp rootwait elevator=deadline" > $TMP_DIR/rootfs/boot/cmdline.txt

echo "Backing up to $BACKUP_DIR"
$SCRIPT_DIR/backup.sh $BACKUP_DIR
echo "Backed up"

echo "Removing old files"
rm -rf $NFS_DIR/$NFS_SUB_NAME

echo "Copying new files"
cp -r $TMP_DIR/rootfs $NFS_DIR/$NFS_SUB_NAME
rm -rf $TMP_DIR
echo "Update done"
