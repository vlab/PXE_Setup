#!/bin/bash
source $(dirname $0)/config.source
# Install nfs server and tftpd
echo "Installing nfs server"
apt install -y nfs-kernel-server tftpd-hpa dnsmasq
# Setup nfs
echo "Setting up NFS"
mkdir -p $NFS_DIR/$NFS_SUB_NAME/boot
mkdir -p $TFTPBOOT_DIR

echo "Setting up exports"
echo "$NFS_DIR *(rw,sync,no_subtree_check,no_root_squash,insecure)" >> /etc/exports

echo "Disabling nfs-server"
systemctl stop nfs-server
systemctl disable nfs-server
echo "Done"
