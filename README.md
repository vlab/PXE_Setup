# PXE_Server
This repository has shell scripts for setting up an NFS and tftp server for use with PXE.

## config.source
Here are all the configs stored which are shared between scripts.
- NFS_DIR: Defines which directory to use for the NFS share
- NFS_SUB_NAME: Defines under which name the files for the rootfs are stored
- TFTPBOOT_DIR: Defines where to start the tftp server
- ETH_IFACE: Defines on which interface dnsmasq should be bound
- ETH_RANGE: Defines the broadcast address used by dnsmasq
- NFS_HOST: Defines the ip address of the NFS share (Should be the same as ETH_IFACE)
- PXE_NAME: The name dnsmasq should advertise this PXE server as


## setup.sh (Debian/Ubuntu)
This script installs all the required packages and creates the necessary files accroding to `config.source`.

## start.sh
Starts tftp, dnsmasq and nfs-server.

## setup_new_rpi.sh
Mounts the required boot files for a given serial number (first argument).

## update.sh
Copies the required files from the meta-vlab.

## backup.sh
Copies all files from the nfs share to a directory.
