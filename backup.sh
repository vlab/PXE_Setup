#!/bin/bash
source $(dirname $0)/config.source
if [ ! $# -eq 1 ]; then
	echo "Usage: $0 <Backup Dir>"
	exit 1
fi
BACKUP_DIR=$1
# Check if Backup Dir exists if not create it
if [ ! -d $(dirname $BACKUP_DIR) ]; then
  mkdir -p $(dirname $BACKUP_DIR)
fi

cp -r $NFS_DIR/$NFS_SUB_NAME $BACKUP_DIR
