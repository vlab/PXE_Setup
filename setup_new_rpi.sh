#!/bin/bash
source $(dirname $0)/config.source
if [ ! $# -eq 1 ]; then
	echo "Usage: $0 <RPI serial number>"
	exit 1
fi
RPI_SER_NUM=$1

echo "Creating Directory for $RPI_SER_NUM"
sudo mkdir -p $TFTPBOOT_DIR/$RPI_SER_NUM

echo "Mounting $RPI_SER_NUM"
sudo mount --bind $NFS_DIR/$NFS_SUB_NAME/boot $TFTPBOOT_DIR/$RPI_SER_NUM
